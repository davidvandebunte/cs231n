from builtins import range
from builtins import object
import numpy as np
import functools

from cs231n.layers import *
from cs231n.layer_utils import *

from cs231n.layers import affine_forward, affine_backward, \
                          relu_forward, relu_backward, \
                          batchnorm_forward, batchnorm_backward_alt, \
                          layernorm_forward, layernorm_backward, \
                          dropout_forward, dropout_backward
from cs231n.layer_utils import affine_relu_forward, affine_relu_backward

class TwoLayerNet(object):
    """
    A two-layer fully-connected neural network with ReLU nonlinearity and
    softmax loss that uses a modular layer design. We assume an input dimension
    of D, a hidden dimension of H, and perform classification over C classes.

    The architecure should be affine - relu - affine - softmax.

    Note that this class does not implement gradient descent; instead, it
    will interact with a separate Solver object that is responsible for running
    optimization.

    The learnable parameters of the model are stored in the dictionary
    self.params that maps parameter names to numpy arrays.
    """

    def __init__(self, input_dim=3*32*32, hidden_dim=100, num_classes=10,
                 weight_scale=1e-3, reg=0.0):
        """
        Initialize a new network.

        Inputs:
        - input_dim: An integer giving the size of the input
        - hidden_dim: An integer giving the size of the hidden layer
        - num_classes: An integer giving the number of classes to classify
        - weight_scale: Scalar giving the standard deviation for random
          initialization of the weights.
        - reg: Scalar giving L2 regularization strength.
        """
        self.params = {}
        self.reg = reg

        ############################################################################
        # TODO: Initialize the weights and biases of the two-layer net. Weights    #
        # should be initialized from a Gaussian centered at 0.0 with               #
        # standard deviation equal to weight_scale, and biases should be           #
        # initialized to zero. All weights and biases should be stored in the      #
        # dictionary self.params, with first layer weights                         #
        # and biases using the keys 'W1' and 'b1' and second layer                 #
        # weights and biases using the keys 'W2' and 'b2'.                         #
        ############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        self.params['W1'] = weight_scale * np.random.standard_normal((input_dim, hidden_dim))
        self.params['b1'] = np.zeros((hidden_dim))
        self.params['W2'] = weight_scale * np.random.standard_normal((hidden_dim, num_classes))
        self.params['b2'] = np.zeros((num_classes))

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                             END OF YOUR CODE                             #
        ############################################################################


    def loss(self, X, y=None):
        """
        Compute loss and gradient for a minibatch of data.

        Inputs:
        - X: Array of input data of shape (N, d_1, ..., d_k)
        - y: Array of labels, of shape (N,). y[i] gives the label for X[i].

        Returns:
        If y is None, then run a test-time forward pass of the model and return:
        - scores: Array of shape (N, C) giving classification scores, where
          scores[i, c] is the classification score for X[i] and class c.

        If y is not None, then run a training-time forward and backward pass and
        return a tuple of:
        - loss: Scalar value giving the loss
        - grads: Dictionary with the same keys as self.params, mapping parameter
          names to gradients of the loss with respect to those parameters.
        """
        scores = None
        ############################################################################
        # TODO: Implement the forward pass for the two-layer net, computing the    #
        # class scores for X and storing them in the scores variable.              #
        ############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        aff_relu, aff_relu_cache = affine_relu_forward(
            X, self.params['W1'], self.params['b1']
        )
        scores, aff_cache = affine_forward(
            aff_relu, self.params['W2'], self.params['b2']
        )

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                             END OF YOUR CODE                             #
        ############################################################################

        # If y is None then we are in test mode so just return scores
        if y is None:
            return scores

        loss, grads = 0, {}
        ############################################################################
        # TODO: Implement the backward pass for the two-layer net. Store the loss  #
        # in the loss variable and gradients in the grads dictionary. Compute data #
        # loss using softmax, and make sure that grads[k] holds the gradients for  #
        # self.params[k]. Don't forget to add L2 regularization!                   #
        #                                                                          #
        # NOTE: To ensure that your implementation matches ours and you pass the   #
        # automated tests, make sure that your L2 regularization includes a factor #
        # of 0.5 to simplify the expression for the gradient.                      #
        ############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        sm_loss, dsoftmax = softmax_loss(scores, y)
        dscores, daff_W2, daff_b2 = \
            affine_backward(dsoftmax, aff_cache)
        _, daff_relu_W1, daff_relu_b1 = \
            affine_relu_backward(dscores, aff_relu_cache)

        reg1_loss = 0.5 * self.reg * np.sum(np.power(self.params['W2'], 2))
        reg2_loss = 0.5 * self.reg * np.sum(np.power(self.params['W1'], 2))
        loss = sm_loss + reg1_loss + reg2_loss

        dreg_W1 = self.reg * self.params['W1']
        dreg_W2 = self.reg * self.params['W2']

        grads['W2'] = daff_W2 + dreg_W2
        grads['b2'] = daff_b2
        grads['W1'] = daff_relu_W1 + dreg_W1
        grads['b1'] = daff_relu_b1

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                             END OF YOUR CODE                             #
        ############################################################################

        return loss, grads


def affine_bn_relu_forward(xin, w, b, gamma, beta, bn_param):
    aff_out, aff_cache = affine_forward(xin, w, b)
    bn_out, bn_cache = batchnorm_forward(aff_out, gamma, beta, bn_param)
    relu_out, relu_cache = relu_forward(bn_out)
    cache = (aff_cache, bn_cache, relu_cache)
    return relu_out, cache


def affine_bn_relu_backward(dout, cache):
    """None"""
    aff_cache, bn_cache, relu_cache = cache
    dbn = relu_backward(dout, relu_cache)
    daff, dgamma, dbeta = batchnorm_backward_alt(dbn, bn_cache)
    dx, dw, db = affine_backward(daff, aff_cache)
    return dx, dw, db, dgamma, dbeta


def affine_ln_relu_forward(xin, w, b, gamma, beta, ln_param):
    aff_out, aff_cache = affine_forward(xin, w, b)
    ln_out, ln_cache = layernorm_forward(aff_out, gamma, beta, ln_param)
    relu_out, relu_cache = relu_forward(ln_out)
    cache = (aff_cache, ln_cache, relu_cache)
    return relu_out, cache


def affine_ln_relu_backward(dout, cache):
    """None"""
    aff_cache, ln_cache, relu_cache = cache
    dln = relu_backward(dout, relu_cache)
    daff, dgamma, dbeta = layernorm_backward(dln, ln_cache)
    dx, dw, db = affine_backward(daff, aff_cache)
    return dx, dw, db, dgamma, dbeta


class FullyConnectedNet(object):
    """
    A fully-connected neural network with an arbitrary number of hidden layers,
    ReLU nonlinearities, and a softmax loss function. This will also implement
    dropout and batch/layer normalization as options. For a network with L layers,
    the architecture will be

    {affine - [batch/layer norm] - relu - [dropout]} x (L - 1) - affine - softmax

    where batch/layer normalization and dropout are optional, and the {...} block is
    repeated L - 1 times.

    Similar to the TwoLayerNet above, learnable parameters are stored in the
    self.params dictionary and will be learned using the Solver class.
    """

    def __init__(self, hidden_dims, input_dim=3*32*32, num_classes=10,
                 dropout=1, normalization=None, reg=0.0,
                 weight_scale=1e-2, dtype=np.float32, seed=None):
        """
        Initialize a new FullyConnectedNet.

        Inputs:
        - hidden_dims: A list of integers giving the size of each hidden layer.
        - input_dim: An integer giving the size of the input.
        - num_classes: An integer giving the number of classes to classify.
        - dropout: Scalar between 0 and 1 giving dropout strength. If dropout=1 then
          the network should not use dropout at all.
        - normalization: What type of normalization the network should use. Valid values
          are "batchnorm", "layernorm", or None for no normalization (the default).
        - reg: Scalar giving L2 regularization strength.
        - weight_scale: Scalar giving the standard deviation for random
          initialization of the weights.
        - dtype: A numpy datatype object; all computations will be performed using
          this datatype. float32 is faster but less accurate, so you should use
          float64 for numeric gradient checking.
        - seed: If not None, then pass this random seed to the dropout layers. This
          will make the dropout layers deterministic so we can gradient check the
          model.
        """
        self.normalization = normalization
        self.use_dropout = dropout != 1
        self.reg = reg
        self.num_layers = 1 + len(hidden_dims)
        self.dtype = dtype
        self.params = {}

        ############################################################################
        # TODO: Initialize the parameters of the network, storing all values in    #
        # the self.params dictionary. Store weights and biases for the first layer #
        # in W1 and b1; for the second layer use W2 and b2, etc. Weights should be #
        # initialized from a normal distribution centered at 0 with standard       #
        # deviation equal to weight_scale. Biases should be initialized to zero.   #
        #                                                                          #
        # When using batch normalization, store scale and shift parameters for the #
        # first layer in gamma1 and beta1; for the second layer use gamma2 and     #
        # beta2, etc. Scale parameters should be initialized to ones and shift     #
        # parameters should be initialized to zeros.                               #
        ############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        W_params = {f'W{i+2}': weight_scale * np.random.standard_normal((hidden_dims[i], hidden_dims[i+1])) \
                for i in range(self.num_layers - 2) }
        b_params = {f'b{i+1}': np.zeros((hidden_dims[i])) \
                for i in range(self.num_layers - 1) }
        self.params = {**W_params, **b_params}
        self.params['W1'] = weight_scale * np.random.standard_normal((input_dim, hidden_dims[0]))
        self.params[f'W{self.num_layers}'] = weight_scale * \
                np.random.standard_normal((hidden_dims[-1], num_classes))
        self.params[f'b{self.num_layers}'] = np.zeros((num_classes))

        if self.normalization == 'batchnorm' or self.normalization == 'layernorm':
            gamma_params = {f'gamma{i+1}': np.ones(hidden_dims[i])
                            for i in range(self.num_layers - 1)}
            beta_params = {f'beta{i+1}': np.zeros(hidden_dims[i])
                           for i in range(self.num_layers - 1)}
            self.params = {**self.params, **gamma_params, **beta_params}
        else:
            assert not self.normalization

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                             END OF YOUR CODE                             #
        ############################################################################

        # When using dropout we need to pass a dropout_param dictionary to each
        # dropout layer so that the layer knows the dropout probability and the mode
        # (train / test). You can pass the same dropout_param to each dropout layer.
        self.dropout_param = {}
        if self.use_dropout:
            self.dropout_param = {'mode': 'train', 'p': dropout}
            if seed is not None:
                self.dropout_param['seed'] = seed

        # With batch normalization we need to keep track of running means and
        # variances, so we need to pass a special bn_param object to each batch
        # normalization layer. You should pass self.bn_params[0] to the forward pass
        # of the first batch normalization layer, self.bn_params[1] to the forward
        # pass of the second batch normalization layer, etc.
        self.bn_params = []
        if self.normalization=='batchnorm':
            self.bn_params = [{'mode': 'train'} for i in range(self.num_layers - 1)]
        if self.normalization=='layernorm':
            self.bn_params = [{} for i in range(self.num_layers - 1)]

        # Cast all parameters to the correct datatype
        for k, v in self.params.items():
            self.params[k] = v.astype(dtype)


    def loss(self, X, y=None):
        """
        Compute loss and gradient for the fully-connected net.

        Input / output: Same as TwoLayerNet above.
        """
        X = X.astype(self.dtype)
        mode = 'test' if y is None else 'train'

        # Set train/test mode for batchnorm params and dropout param since they
        # behave differently during training and testing.
        if self.use_dropout:
            self.dropout_param['mode'] = mode
        if self.normalization=='batchnorm':
            for bn_param in self.bn_params:
                bn_param['mode'] = mode
        scores = None
        ############################################################################
        # TODO: Implement the forward pass for the fully-connected net, computing  #
        # the class scores for X and storing them in the scores variable.          #
        #                                                                          #
        # When using dropout, you'll need to pass self.dropout_param to each       #
        # dropout forward pass.                                                    #
        #                                                                          #
        # When using batch normalization, you'll need to pass self.bn_params[0] to #
        # the forward pass for the first batch normalization layer, pass           #
        # self.bn_params[1] to the forward pass for the second batch normalization #
        # layer, etc.                                                              #
        ############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        # Hidden layers
        def feedforward_layer(state, parameters, forward_func):
            xin, caches = state
            xout, cache = forward_func(xin, *parameters)
            if self.use_dropout:
                xout, dropout_cache = dropout_forward(xout, self.dropout_param)
                cache = (dropout_cache, cache)
            caches.append(cache)
            return (xout, caches)

        w_params = [self.params[f'W{idx+1}']
                    for idx in range(self.num_layers - 1)]
        b_params = [self.params[f'b{idx+1}']
                    for idx in range(self.num_layers - 1)]

        if self.normalization == 'batchnorm':
            gamma_params = [self.params[f'gamma{idx+1}']
                            for idx in range(self.num_layers - 1)]
            beta_params = [self.params[f'beta{idx+1}']
                           for idx in range(self.num_layers - 1)]
            parameters = zip(w_params,
                             b_params,
                             gamma_params,
                             beta_params,
                             self.bn_params)
            forward_func = affine_bn_relu_forward
        elif self.normalization == 'layernorm':
            gamma_params = [self.params[f'gamma{idx+1}']
                            for idx in range(self.num_layers - 1)]
            beta_params = [self.params[f'beta{idx+1}']
                           for idx in range(self.num_layers - 1)]
            parameters = zip(w_params,
                             b_params,
                             gamma_params,
                             beta_params,
                             self.bn_params)
            forward_func = affine_ln_relu_forward
        else:
            assert not self.normalization
            parameters = zip(w_params,
                             b_params)
            forward_func = affine_relu_forward

        X, caches = functools.reduce(functools.partial(feedforward_layer,
                                                       forward_func=forward_func),
                                     parameters,
                                     (X, []))

        # Output layer
        scores, scores_cache = affine_forward(
            X, self.params[f'W{self.num_layers}'], self.params[f'b{self.num_layers}']
        )

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                             END OF YOUR CODE                             #
        ############################################################################

        # If test mode return early
        if mode == 'test':
            return scores

        loss, grads = 0.0, {}
        ############################################################################
        # TODO: Implement the backward pass for the fully-connected net. Store the #
        # loss in the loss variable and gradients in the grads dictionary. Compute #
        # data loss using softmax, and make sure that grads[k] holds the gradients #
        # for self.params[k]. Don't forget to add L2 regularization!               #
        #                                                                          #
        # When using batch/layer normalization, you don't need to regularize the scale   #
        # and shift parameters.                                                    #
        #                                                                          #
        # NOTE: To ensure that your implementation matches ours and you pass the   #
        # automated tests, make sure that your L2 regularization includes a factor #
        # of 0.5 to simplify the expression for the gradient.                      #
        ############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        sm_loss, dsoftmax = softmax_loss(scores, y)
        current_layer = self.num_layers
        dscores, dscores_W, grads[f'b{current_layer}'] = affine_backward(dsoftmax, scores_cache)

        def individual_reg_loss(total_reg_loss, idx):
            return total_reg_loss + 0.5 * self.reg * np.sum(np.power(self.params[f'W{idx+1}'], 2))
        reg_loss = functools.reduce(individual_reg_loss, range(self.num_layers), 0)

        if self.normalization == 'batchnorm':
            def backward_pass(state, cache):
                current_layer, dout, daff_ws, grads = state
                if self.use_dropout:
                    dropout_cache, cache = cache
                    dout = dropout_backward(dout, dropout_cache)
                daff_x2, daff_w, \
                    grads[f'b{current_layer}'], \
                    grads[f'gamma{current_layer}'], \
                    grads[f'beta{current_layer}'] = \
                    affine_bn_relu_backward(dout, cache)
                daff_ws.append(daff_w)
                return (current_layer - 1, daff_x2, daff_ws, grads)
        elif self.normalization == 'layernorm':
            def backward_pass(state, cache):
                current_layer, dout, daff_ws, grads = state
                if self.use_dropout:
                    dropout_cache, cache = cache
                    dout = dropout_backward(dout, dropout_cache)
                daff_x2, daff_w, \
                    grads[f'b{current_layer}'], \
                    grads[f'gamma{current_layer}'], \
                    grads[f'beta{current_layer}'] = \
                    affine_ln_relu_backward(dout, cache)
                daff_ws.append(daff_w)
                return (current_layer - 1, daff_x2, daff_ws, grads)
        else:
            assert not self.normalization
            def backward_pass(state, cache):
                current_layer, dout, daff_ws, grads = state
                if self.use_dropout:
                    dropout_cache, cache = cache
                    dout = dropout_backward(dout, dropout_cache)
                daff_x2, daff_w, grads[f'b{current_layer}'] = \
                    affine_relu_backward(dout, cache)
                daff_ws.append(daff_w)
                return (current_layer - 1, daff_x2, daff_ws, grads)

        _, _, daff_ws, grads = functools.reduce(
            backward_pass,
            reversed(caches),
            (current_layer - 1, dscores, [dscores_W], grads))

        loss = sm_loss + reg_loss

        dreg_ws = [self.reg * self.params[f'W{idx+1}']
                   for idx in range(self.num_layers)]
        zipped_dWs = enumerate(zip(reversed(daff_ws), dreg_ws))
        W_grads = {f'W{idx+1}': sum(two_dWs) for idx, two_dWs in zipped_dWs}
        grads = {**W_grads, **grads}

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                             END OF YOUR CODE                             #
        ############################################################################

        return loss, grads
