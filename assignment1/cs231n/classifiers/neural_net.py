from __future__ import print_function

from builtins import range
from builtins import object
import numpy as np
import matplotlib.pyplot as plt
from past.builtins import xrange


class TwoLayerNet(object):
    """
    A two-layer fully-connected neural network. The net has an input dimension of
    N, a hidden layer dimension of H, and performs classification over C classes.
    We train the network with a softmax loss function and L2 regularization on the
    weight matrices. The network uses a ReLU nonlinearity after the first fully
    connected layer.

    In other words, the network has the following architecture:

    input - fully connected layer - ReLU - fully connected layer - softmax

    The outputs of the second fully-connected layer are the scores for each class.
    """
    def __init__(self, input_size, hidden_size, output_size, std=1e-4):
        """
        Initialize the model. Weights are initialized to small random values and
        biases are initialized to zero. Weights and biases are stored in the
        variable self.params, which is a dictionary with the following keys:

        W1: First layer weights; has shape (D, H)
        b1: First layer biases; has shape (H,)
        W2: Second layer weights; has shape (H, C)
        b2: Second layer biases; has shape (C,)

        Inputs:
        - input_size: The dimension D of the input data.
        - hidden_size: The number of neurons H in the hidden layer.
        - output_size: The number of classes C.
        """
        self.params = {}
        self.params['W1'] = std * np.random.randn(input_size, hidden_size)
        self.params['b1'] = np.zeros(hidden_size)
        self.params['W2'] = std * np.random.randn(hidden_size, output_size)
        self.params['b2'] = np.zeros(output_size)

    def loss(self, X, y=None, reg=0.0):
        """
        Compute the loss and gradients for a two layer fully connected neural
        network.

        Inputs:
        - X: Input data of shape (N, D). Each X[i] is a training sample.
        - y: Vector of training labels. y[i] is the label for X[i], and each y[i] is
          an integer in the range 0 <= y[i] < C. This parameter is optional; if it
          is not passed then we only return scores, and if it is passed then we
          instead return the loss and gradients.
        - reg: Regularization strength.

        Returns:
        If y is None, return a matrix scores of shape (N, C) where scores[i, c] is
        the score for class c on input X[i].

        If y is not None, instead return a tuple of:
        - loss: Loss (data loss and regularization loss) for this batch of training
          samples.
        - grads: Dictionary mapping parameter names to gradients of those parameters
          with respect to the loss function; has the same keys as self.params.
        """
        # Unpack variables from the params dictionary
        W1, b1 = self.params['W1'], self.params['b1']
        W2, b2 = self.params['W2'], self.params['b2']
        N, D = X.shape
        H, C = W2.shape

        # Compute the forward pass
        scores = None
        #############################################################################
        # TODO: Perform the forward pass, computing the class scores for the input. #
        # Store the result in the scores variable, which should be an array of      #
        # shape (N, C).                                                             #
        #############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        # (N, H) from: (N, D) dot (D, H), then broadcast with b1.
        h1 = np.dot(X, W1) + b1
        relu = h1
        relu[h1 < 0] = 0.0
        # (N, C) from: (N, H) dot (H, C), then broadcast with b2.
        scores = np.dot(relu, W2) + b2

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        # If the targets are not given then jump out, we're done
        if y is None:
            return scores

        # Compute the loss
        loss = None
        #############################################################################
        # TODO: Finish the forward pass, and compute the loss. This should include  #
        # both the data loss and L2 regularization for W1 and W2. Store the result  #
        # in the variable loss, which should be a scalar. Use the Softmax           #
        # classifier loss.                                                          #
        #############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        # TODO: Backprop might be easier with the other form of cross-entropy loss.

        # (C, N)
        unnormalized_probabilities = np.exp(scores).T
        # (N,)
        sum_unnormalized = np.sum(unnormalized_probabilities, axis=0)
        # (N,)
        unnormalized_probabilities_y = unnormalized_probabilities[y, np.arange(N)]
        # (N,)
        inv_sum_unnormalized = 1 / sum_unnormalized
        # (N,)
        normalized_probabilities_y = unnormalized_probabilities_y * inv_sum_unnormalized
        # (N,)
        total_data_loss = -np.sum(np.log(normalized_probabilities_y))
        data_loss = total_data_loss / N
        reg_loss1 = reg * np.sum(W1 * W1)
        reg_loss2 = reg * np.sum(W2 * W2)
        loss = data_loss + reg_loss1 + reg_loss2

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        # Backward pass: compute gradients
        grads = {}
        #############################################################################
        # TODO: Compute the backward pass, computing the derivatives of the weights #
        # and biases. Store the results in the grads dictionary. For example,       #
        # grads['W1'] should store the gradient on W1, and be a matrix of same size #
        #############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        # dloss / dW2 of shape (D, H)
        grads['W2'] = reg * 2 * W2
        grads['W1'] = reg * 2 * W1
        # = (ddata_loss / dtotal_data_loss) * (dloss / ddata_loss)
        dtotal_data_loss = (1 / N) * 1.0
        # = (dtotal_data_loss / dnormalized_probabilities_y) * (dloss / dtotal_data_loss)
        dnormalized_probabilities_y = (-1.0 / normalized_probabilities_y) * dtotal_data_loss
        # = (dnormalized_probabilities_y / dunnormalized_probabilities_y)
        #                       * (dloss / dnormalized_probabilities_y)
        dunnormalized_probabilities_y = inv_sum_unnormalized * dnormalized_probabilities_y
        # = (dnormalized_probabilities_y / dinv_sum_unnormalized)
        #                       * (dloss / dnormalized_probabilities_y)
        dinv_sum_unnormalized = unnormalized_probabilities_y * dnormalized_probabilities_y
        # = (dinv_sum_unnormalized / dsum_unnormalized)
        #                 * (dloss / dinv_sum_unnormalized)
        dsum_unnormalized = (-1.0 / np.power(sum_unnormalized, 2)) * dinv_sum_unnormalized
        # = (dsum_unnormalized / dunnormalized_probabilities) * (dloss / dsum_unnormalized)
        dunnormalized_probabilities = np.ones((C, N)) * dsum_unnormalized
        # = (dunnormalized_probabilities_y / dunnormalized_probabilities[y, np.arangs(N)])
        #                         * (dloss / dunnormalized_probabilities_y)
        dunnormalized_probabilities[y, np.arange(N)] += dunnormalized_probabilities_y
        # = (dunnormalized_probabilities / dscores) * (dloss / dunnormalized_probabilities)
        dscores = (np.exp(scores).T * dunnormalized_probabilities).T

        # = (dscores / db2) * (dloss / dscores)
        grads['b2'] = np.sum(1.0 * dscores, axis=0)
        # = (dscores / dW2) * (dloss / dscores)
        # (H, C) from: (H, N) dot (N, C).
        grads['W2'] += relu.T.dot(dscores)
        # = (dscores / drelu) * (dloss / dscores)
        # (N, H) from: (N, C) dot (C, H).
        drelu = dscores.dot(W2.T)
        # = (drelu / dh1) * (dloss / drelu)
        active_relu = np.ones((N, H))
        active_relu[h1 == 0] = 0
        dh1 = active_relu * drelu
        # = (dh1 / db1) * (dloss / dh1)
        grads['b1'] = np.sum(1.0 * dh1, axis=0)
        # = (dh1 / dW1) * (dloss / dh1)
        # (D, H) from: (D, N) dot (N, H).
        grads['W1'] += X.T.dot(dh1)

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        return loss, grads

    def train(self,
              X,
              y,
              X_val,
              y_val,
              learning_rate=1e-3,
              learning_rate_decay=0.95,
              reg=5e-6,
              num_iters=100,
              batch_size=200,
              verbose=False):
        """
        Train this neural network using stochastic gradient descent.

        Inputs:
        - X: A numpy array of shape (N, D) giving training data.
        - y: A numpy array f shape (N,) giving training labels; y[i] = c means that
          X[i] has label c, where 0 <= c < C.
        - X_val: A numpy array of shape (N_val, D) giving validation data.
        - y_val: A numpy array of shape (N_val,) giving validation labels.
        - learning_rate: Scalar giving learning rate for optimization.
        - learning_rate_decay: Scalar giving factor used to decay the learning rate
          after each epoch.
        - reg: Scalar giving regularization strength.
        - num_iters: Number of steps to take when optimizing.
        - batch_size: Number of training examples to use per step.
        - verbose: boolean; if true print progress during optimization.
        """
        num_train = X.shape[0]
        iterations_per_epoch = max(num_train / batch_size, 1)

        # Use SGD to optimize the parameters in self.model
        loss_history = []
        train_acc_history = []
        val_acc_history = []

        for it in range(num_iters):
            X_batch = None
            y_batch = None

            #########################################################################
            # TODO: Create a random minibatch of training data and labels, storing  #
            # them in X_batch and y_batch respectively.                             #
            #########################################################################
            # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

            indices = np.random.choice(num_train, batch_size, replace=True)
            X_batch = X[indices]
            y_batch = y[indices]

            # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

            # Compute loss and gradients using the current minibatch
            loss, grads = self.loss(X_batch, y=y_batch, reg=reg)
            loss_history.append(loss)

            #########################################################################
            # TODO: Use the gradients in the grads dictionary to update the         #
            # parameters of the network (stored in the dictionary self.params)      #
            # using stochastic gradient descent. You'll need to use the gradients   #
            # stored in the grads dictionary defined above.                         #
            #########################################################################
            # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

            self.params['W1'] -= learning_rate * grads['W1']
            self.params['b1'] -= learning_rate * grads['b1']
            self.params['W2'] -= learning_rate * grads['W2']
            self.params['b2'] -= learning_rate * grads['b2']

            # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

            if verbose and it % 100 == 0:
                print('iteration %d / %d: loss %f' % (it, num_iters, loss))

            # Every epoch, check train and val accuracy and decay learning rate.
            if it % iterations_per_epoch == 0:
                # Check accuracy
                train_acc = (self.predict(X_batch) == y_batch).mean()
                val_acc = (self.predict(X_val) == y_val).mean()
                train_acc_history.append(train_acc)
                val_acc_history.append(val_acc)

                # Decay learning rate
                learning_rate *= learning_rate_decay

        return {
            'loss_history': loss_history,
            'train_acc_history': train_acc_history,
            'val_acc_history': val_acc_history,
        }

    def predict(self, X):
        """
        Use the trained weights of this two-layer network to predict labels for
        data points. For each data point we predict scores for each of the C
        classes, and assign each data point to the class with the highest score.

        Inputs:
        - X: A numpy array of shape (N, D) giving N D-dimensional data points to
          classify.

        Returns:
        - y_pred: A numpy array of shape (N,) giving predicted labels for each of
          the elements of X. For all i, y_pred[i] = c means that X[i] is predicted
          to have class c, where 0 <= c < C.
        """
        y_pred = None

        ###########################################################################
        # TODO: Implement this function; it should be VERY simple!                #
        ###########################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        y_pred = np.argmax(self.loss(X), axis=1)

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        return y_pred
