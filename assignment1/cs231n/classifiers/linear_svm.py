from builtins import range
import numpy as np
from random import shuffle
from past.builtins import xrange

def svm_loss_naive(W, X, y, reg):
    """
    Structured SVM loss function, naive implementation (with loops).

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
      that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    dW = np.zeros(W.shape)  # initialize the gradient as zero

    # compute the loss and the gradient
    num_classes = W.shape[1]
    num_train = X.shape[0]
    loss = 0.0
    for i in range(num_train):
        scores = X[i].dot(W)
        correct_class_score = scores[y[i]]
        missed_margin = 0
        for j in range(num_classes):
            if j == y[i]:
                continue
            margin = scores[j] - correct_class_score + 1  # note delta = 1
            if margin > 0:
                loss += margin
                missed_margin = missed_margin + 1
                dW[:, j] = dW[:, j] + X[i, :]
        dW[:, y[i]] = dW[:, y[i]] - missed_margin * X[i, :]

    # Right now the loss is a sum over all training examples, but we want it
    # to be an average instead so we divide by num_train.
    loss /= num_train

    # Add regularization to the loss.
    loss += reg * np.sum(W * W)

    #############################################################################
    # TODO:                                                                     #
    # Compute the gradient of the loss function and store it dW.                #
    # Rather that first computing the loss and then computing the derivative,   #
    # it may be simpler to compute the derivative at the same time that the     #
    # loss is being computed. As a result you may need to modify some of the    #
    # code above to compute the gradient.                                       #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    dW = dW / num_train
    dW = dW + reg * 2 * W

    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
    
    return loss, dW



def svm_loss_vectorized(W, X, y, reg):
    """
    Structured SVM loss function, vectorized implementation.

    Inputs and outputs are the same as svm_loss_naive.
    """
    loss = 0.0
    dW = np.zeros(W.shape)  # initialize the gradient as zero

    #############################################################################
    # TODO:                                                                     #
    # Implement a vectorized version of the structured SVM loss, storing the    #
    # result in loss.                                                           #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    delta = 1
    num_train = X.shape[0]
#    num_classes = W.shape[1]
#    loss = 0.0
#    for i in range(num_train):
#        scores = X[i].dot(W)
#        margins = scores[np.arange(len(scores)) != y[i]] - scores[y[i]] + delta
#        cls_loss = margins[margins > 0]
#        loss += np.sum(cls_loss)

    # (C, N) -> Columns of class scores, column per minibatch training sample.
    scores = X.dot(W).transpose()
    # Broadcast scores[y[i]] via subtraction, zero out some values afterwards.
    margins = scores - scores[y, np.arange(num_train)] + delta
    margins[y, np.arange(num_train)] = 0
    cls_loss = margins[margins > 0]
    loss = np.sum(cls_loss)

    # Right now the loss is a sum over all training examples, but we want it
    # to be an average instead so we divide by num_train.
    loss /= num_train

    # Add regularization to the loss.
    loss += reg * np.sum(W * W)

    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    #############################################################################
    # TODO:                                                                     #
    # Implement a vectorized version of the gradient for the structured SVM     #
    # loss, storing the result in dW.                                           #
    #                                                                           #
    # Hint: Instead of computing the gradient from scratch, it may be easier    #
    # to reuse some of the intermediate values that you used to compute the     #
    # loss.                                                                     #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    # A (C, N) matrix with a bunch of zeros and ones where the ones are where
    # we need to add a D dimensional training vector.
    misses = np.zeros(margins.shape)
    misses[margins > 0] = 1.0

    # TODO: Add in minus times the number of misses?
    # (,N) elements with the number of misses per training example.
    total_misses = np.sum(misses, axis=0)
    misses[y, np.arange(num_train)] = -total_misses

    # Construct a (C, N, D) ndarray and then compress the `N` dimension with a
    # sum.
    #
    # Take the (C, N) view of zeros/ones and broadcast multiply with the (N, D)
    # array X.
    # (C, N, 1)
    # (1, N, D)
    # (C, N, D)
    full_results = misses[..., np.newaxis] * X
    # (C, D) transpose to (D, C)
    dW = np.sum(full_results, axis=1).transpose()

    dW = dW / num_train
    dW = dW + reg * 2 * W

    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    return loss, dW
