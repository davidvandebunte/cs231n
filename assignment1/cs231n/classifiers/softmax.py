from builtins import range
import numpy as np
from random import shuffle
from past.builtins import xrange


def softmax_loss_naive(W, X, y, reg):
    """
    Softmax loss function, naive implementation (with loops)

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
      that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using explicit loops.     #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    def softargmax(z):
        return np.exp(z) / sum(np.exp(z))

    num_classes = W.shape[1]
    num_train = X.shape[0]
    for i in range(num_train):
        scores = X[i].dot(W)
        scores = scores - max(scores)
        scores = softargmax(scores)
        loss = loss - np.log(scores[y[i]])
        for j in range(num_classes):
            if j == y[i]:
                dW[:, j] += (scores[j] - 1) * X[i, :]
            else:
                dW[:, j] += scores[j] * X[i, :]

    loss /= num_train
    loss += reg * np.sum(W * W)

    dW = dW / num_train
    dW = dW + reg * 2 * W

    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
    """
    Softmax loss function, vectorized version.

    Inputs and outputs are the same as softmax_loss_naive.
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    num_train = X.shape[0]
    # (C, N)
    scores = X.dot(W).transpose()
    unnormalized_probabilites = np.exp(scores)
    # (N,)
    sum_unnormalized = np.sum(unnormalized_probabilites, axis=0)
    # (C, N) after broadcast division
    normalized_probabilities = unnormalized_probabilites / sum_unnormalized
    loss = -np.sum(np.log(normalized_probabilities[y, np.arange(num_train)]))
    loss /= num_train
    normalized_probabilities[y, np.arange(num_train)] -= 1
    # X is (N, D) and normalized_probabilities is (C, N); need dW to be (D, C)
    dW = normalized_probabilities.dot(X).transpose()
    dW /= num_train
    dW = dW + reg * 2 * W

    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    return loss, dW
